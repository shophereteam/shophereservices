package dao

import (
	"errors"
	"fmt"
	"shop-here-api/src/app/modal"
	"strings"
)

type ItemRepo struct {
}

func (*ItemRepo) CreateItem(req modal.Item) (modal.Item, error) {
	// creation with same item name allowed
	// item name cant be empty item yes through an error
	originalRequestObject := req

	if len(strings.ReplaceAll(req.ItemName, " ", "")) < 1 {
		err1 := errors.New("4000")
		return req, err1
	}
	result := Db.Create(&req)
	if result.RowsAffected < 1 {
		return req, result.Error
	}
	ItemTag := modal.ItemTag{ItemID: req.ID, CategoryID: uint(originalRequestObject.PrimaryCategory), IsPrimary: true} // save the primary category

	Db.Create(&ItemTag)

	for _, value := range originalRequestObject.AssociatedCategory {
		if value == originalRequestObject.PrimaryCategory { //check so that primary is not stored as associated also
			continue
		}
		ItemTag = modal.ItemTag{ItemID: req.ID, CategoryID: uint(value), IsPrimary: false} // save the sub categories

		Db.Create(&ItemTag)

	}

	return req, nil
}

func (*ItemRepo) GetAllItem() ([]modal.Item, error) {
	var item []modal.Item

	result := Db.Find(&item) // get all items
	if result.RowsAffected < 1 {
		fmt.Println("no items found in DB...")
		return item, errors.New("3001")
	}
	var category modal.Category
	var categories []modal.Category
	var itemTags []modal.ItemTag
	var UpdatedItem []modal.Item
	for _, oneItem := range item {
		Db.Where("item_id = ?", oneItem.ID).Find(&itemTags)
		for _, oneTag := range itemTags {
			if oneTag.IsPrimary {
				oneItem.PrimaryCategory = int(oneTag.CategoryID)
			} else {
				oneItem.AssociatedCategory = append(oneItem.AssociatedCategory, int(oneTag.CategoryID))
			}

		}
		Db.First(&category, oneItem.PrimaryCategory)
		oneItem.PrimaryCategoryObject = category
		Db.Find(&categories, oneItem.AssociatedCategory)
		oneItem.AssociatedCategoryObject = categories
		UpdatedItem = append(UpdatedItem, oneItem)
	}
	return UpdatedItem, nil
}

// GetItems
/* 	Pagination
@param(pageNo)
*/
func (*ItemRepo) GetItems(pageNo int, pageSize int) ([]modal.Item, error) {
	var items []modal.Item

	if pageNo <= 0 {
		pageNo = 1 // handle edge case
	}

	result := Db.Limit(pageSize).Offset((pageNo - 1) * pageSize).Find(&items) // default pageSize = 10
	if result.RowsAffected < 1 {
		fmt.Println("no items found in DB...")
		return items, errors.New("3001")
	}

	return items, nil
}

func (*ItemRepo) SearchItems(key string) []modal.Item {
	dbSearchKeyword := "%" + key + "%"
	fmt.Println("dbsearchkeyword: ", dbSearchKeyword)

	var items []modal.Item
	//Db.Where("item_name like @name", key).Find(&items) // how to "case in-sensitive" here?
	Db.Raw("select * from items where LOWER(item_name) like LOWER(?)", dbSearchKeyword).Scan(&items)

	return items
}

func (*ItemRepo) DeleteItem(id string) (modal.Item, error) {
	var item modal.Item
	var itemTag modal.ItemTag

	Db.Where("item_id = ?", id).Delete(&itemTag) // delete the item category tags from DB first then delete the item
	result := Db.Delete(&item, id)               // delete item where id matches the PK  (it is a soft delete)
	if result.RowsAffected < 1 {
		fmt.Println("no items found in DB for the given id...")
		return item, errors.New("3002")
	}
	return item, nil
}
func (*ItemRepo) UpdateItem(req modal.Item) (modal.Item, error) {
	// Update with same item name allowed
	// item name can't be empty if yes through an error
	//db:=StartDb()
	//sqlDB,_:=db.DB()
	//defer sqlDB.close()
	originalRequestObject := req

	if len(strings.ReplaceAll(req.ItemName, " ", "")) < 1 {
		err1 := errors.New("4000")
		return req, err1
	}
	resultCheck := Db.Find(&req, req.ID) // first check if item exists if yes then insert otherwise through an error
	if resultCheck.RowsAffected > 0 {
		var itemTag modal.ItemTag
		Db.Where("item_id = ?", req.ID).Delete(&itemTag)                                                                   // delete the item category tags from DB first then create new Item Tags
		ItemTag := modal.ItemTag{ItemID: req.ID, CategoryID: uint(originalRequestObject.PrimaryCategory), IsPrimary: true} // save the primary category

		Db.Create(&ItemTag)
		for _, value := range originalRequestObject.AssociatedCategory {
			if value == originalRequestObject.PrimaryCategory { //check so that primary is not stored as associated also
				continue
			}
			ItemTag = modal.ItemTag{ItemID: req.ID, CategoryID: uint(value), IsPrimary: false} // save the sub categories

			Db.Create(&ItemTag)
		}
		result := Db.Save(&req) // update the table with latest item data
		if result.RowsAffected < 1 {
			return req, result.Error
		}
		return req, nil
	} else {
		return req, errors.New("3002")
	}

}
