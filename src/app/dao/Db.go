package dao

import (
	"fmt"
	"log"
	"os"
	"shop-here-api/src/app/modal"

	"github.com/joho/godotenv"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var Db *gorm.DB
var Err error

func StartDb() *gorm.DB {

	err := godotenv.Load(".env")
	if err != nil {
		log.Fatalf("Error loadng the env variables : %v \n", err)
	}
	// Loading enviroment variables
	//dialect := os.Getenv("DIALECT")
	host := os.Getenv("HOST")
	dbPort := os.Getenv("DBPORT")
	user := os.Getenv("USER")
	dbname := os.Getenv("NAME")
	dbpwd := os.Getenv("PASSWORD")
	// Database connection string
	dbURI := fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable password=%s port=%s", host, user, dbname, dbpwd, dbPort)

	// Opening connection to database
	Db, Err = gorm.Open(postgres.Open(dbURI), &gorm.Config{})
	if Err != nil {
		log.Fatalf("Error connecting to the database  : %v \n", Err)
	} else {
		fmt.Println("Connected to database successfully")
	}
	Db.AutoMigrate(&modal.User{})
	Db.AutoMigrate(&modal.Category{})
	Db.AutoMigrate(&modal.CategoryTag{})
	Db.AutoMigrate(&modal.Item{})
	Db.AutoMigrate(&modal.ItemTag{})
	Db.AutoMigrate(&modal.Order{})
	Db.AutoMigrate(&modal.OrderItem{})
	Db.AutoMigrate(&modal.AssociatedCategory{})


	return Db
}

func GenerateSampleData() bool {
	return true
}
