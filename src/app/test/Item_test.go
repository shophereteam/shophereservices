package test

import (
	"fmt"
	"shop-here-api/src/app/dao"
	"shop-here-api/src/app/modal"
	"strconv"
	"testing"
)

var Item modal.Item = modal.Item{
	ItemName:           "Extension 5",
	Description:        "3m length 1 year warran",
	ImageUrl:           "https://www.google.com/search",
	Price:              500.95,
	DiscountedPrice:    300,
	PrimaryCategory:    1,
	AssociatedCategory: []int{1, 2, 3},
	IsReturnable:       true,
	Tax:                13.09,
	InStock:            true,
	IsFeatured:         true,
	OnSale:             true,
}
var CaseNumber int

func TestCreateItem(t *testing.T) {
	dao.StartDb()
	CaseNumber = 1
	fmt.Printf("Running Test Case:  %v \n", CaseNumber)
	CaseNumber++
	// Item name is valid so it will insert the record and give valid PK value
	object := dao.ItemRepo{}
	itemReturn, _ := object.CreateItem(Item)
	if itemReturn.ID == 0 {
		t.Errorf("TestFailed, got: %d, want: %v .\n", itemReturn.ID, "value more than zero")
	} else {
		fmt.Printf("TestPassed, got: %d, want: %v.\n", itemReturn.ID, "value more than zero")
	}
	fmt.Printf("Running Test Case:  %v \n", CaseNumber)
	CaseNumber++
	// Item name is invalid so it will not insert the record and give zero PK value
	Item.ItemName = " "
	itemReturn, _ = object.CreateItem(Item)
	if itemReturn.ID > 0 {
		t.Errorf("TestFailed, got: %d, want: %v.\n", itemReturn.ID, 0)
	} else {
		fmt.Printf("TestPassed, got: %d, want: %v.\n", itemReturn.ID, 0)
	}
	fmt.Printf("Running Test Case:  %v \n", CaseNumber)
	CaseNumber++
	// Price is invalid so it will not insert the record and give zero PK value
	Item.ItemName = "Valid Item Name"
	Item.Price = -99
	itemReturn, _ = object.CreateItem(Item)
	if itemReturn.ID > 0 {
		t.Errorf("TestFailed, got: %d, want: %v.\n", itemReturn.ID, 0)
	} else {
		fmt.Printf("TestPassed, got: %d, want: %v.\n", itemReturn.ID, 0)
	}
	fmt.Printf("Running Test Case:  %v \n", CaseNumber)
	CaseNumber++
	// Price is valid so it will  insert the record and give valid PK value
	Item.ItemName = "Valid Item Name"
	Item.Price = 99
	itemReturn, _ = object.CreateItem(Item)
	if itemReturn.ID == 0 {
		t.Errorf("TestFailed, got: %d, want: %v.\n", itemReturn.ID, "more than 0 value")
	} else {
		fmt.Printf("TestPassed, got: %d, want: %v.\n", itemReturn.ID, "more than 0 value")
	}
}

func TestUpdateItem(t *testing.T) {
	fmt.Printf("Running Test Case:  %v \n", CaseNumber)
	CaseNumber++
	object := dao.ItemRepo{}
	itemReturn, _ := object.CreateItem(Item) //first create the item and update that Item with name "check name"
	itemReturn.ItemName = "check Item"
	itemReturnUpdate, _ := object.UpdateItem(itemReturn)
	if itemReturnUpdate.ItemName != "check Item" {
		t.Errorf("TestFailed, got: %v, want: %v .\n", itemReturnUpdate.ItemName, "check Item")
	} else if itemReturnUpdate.ItemName == "check Item" {
		fmt.Printf("TestPassed, got: %v, want: %v .\n", itemReturnUpdate.ItemName, "check Item")
	} else {
		t.Errorf("TestFailed....")
	}
	fmt.Printf("Running Test Case:  %v \n", CaseNumber)
	CaseNumber++
	// Price is invalid so it will not update the record and give zero PK value
	Item.Price = -99
	itemReturn, _ = object.UpdateItem(Item)
	if itemReturn.ID > 0 {
		t.Errorf("TestFailed, got: %d, want: %v.\n", itemReturn.ID, 0)
	} else {
		fmt.Printf("TestPassed, got: %d, want: %v.\n", itemReturn.ID, 0)
	}
	fmt.Printf("Running Test Case:  %v \n", CaseNumber)
	CaseNumber++
	// change the Item Id to 0 so it will be not able to update the item in Db
	itemReturn.ID = 0
	itemReturn, err := object.UpdateItem(itemReturn)
	if err != nil {
		fmt.Printf("TestPassed, got: %d, want: %v.\n", itemReturn.ID, 0)
	} else {
		t.Errorf("TestFailed, got: %v, want: %v .\n", itemReturn.ID, "check Item")
	}

	fmt.Printf("Running Test Case:  %v \n", CaseNumber)
	CaseNumber++
	// Price is valid so it will  able to update the record and give valid PK value
	Item.ItemName = "Valid Item Name"
	Item.Price = 99
	itemReturn, _ = object.CreateItem(Item)
	if itemReturn.ID == 0 {
		t.Errorf("TestFailed, got: %d, want: %v.\n", itemReturn.ID, "more than 0")
	} else {
		fmt.Printf("TestPassed, got: %d, want: %v.\n", itemReturn.ID, "more than 0")
	}
}

func TestDeleteItem(t *testing.T) {
	fmt.Printf("Running Test Case:  %v \n", CaseNumber)
	CaseNumber++
	object := dao.ItemRepo{}
	itemReturn, _ := object.CreateItem(Item) //first create the item
	id := strconv.FormatUint(uint64(itemReturn.ID), 10)
	_, err := object.DeleteItem(id)
	// delete the item with the id as given
	if err == nil {
		fmt.Printf("TestPasses, got: %v, want: %v .\n", err, nil)
	} else {
		t.Errorf("TestFailed, got: %v, want: %v.\n", err, nil)
	}
	fmt.Printf("Running Test Case:  %v \n", CaseNumber)
	CaseNumber++
	// try to delete with same id so it will fail saying item not exists
	_, err = object.DeleteItem(id)
	// delete the item with the id as given
	if err == nil {
		t.Errorf("TestFailed, got: %v, want: %v.\n", err, "error code for item not found")
	} else {
		fmt.Printf("TestPasses, got: %v, want: %v .\n", err, "error code for item not found")
	}

}

func TestGetItem(t *testing.T) {
	fmt.Printf("Running Test Case:  %v \n", CaseNumber)
	CaseNumber++
	object := dao.ItemRepo{}
	itemObj, err := object.GetAllItem() //first create the item
	// able to get the item data
	if err == nil {
		fmt.Printf("TestPasses, got: %v, want: %v .\n", len(itemObj), "no of items more than 0")
	} else {
		t.Errorf("TestFailed, got: %v, want: %v .\n", len(itemObj), "no of items more than 0")
	}

}
