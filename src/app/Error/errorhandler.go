package errorhandler

type RError struct {
	ErrorCode  string
	Message    string
	SysMessage string
}

func NewEmptyError() RError {
	return RError{"0", "N/A", "N/A"}
}

func NewError(code string, msg string, sysmsg string) RError {
	return RError{code, msg, sysmsg}
}
