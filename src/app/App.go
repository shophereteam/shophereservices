package app

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	controller "shop-here-api/src/app/controller"
	"shop-here-api/src/app/dao"

	"time"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"gorm.io/gorm"
)

var Db *gorm.DB

func Start() {
	log.Println("API Server starting DB connection...")
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	dao.StartDb()
	//var Db = dao.Db

	/* Close databse connection when the main function closes */
	// defer func(dbs *gorm.DB) {
	// 	err := dbs.Close()
	// 	if err != nil {
	// 		fmt.Println("Database connection successfully closed. Goodbye!")
	// 	}
	// }(Db)

	/*	Make migrations to the dao if they haven't been made already */

	/*	Generate sample DB data if not exists */
	//if(!dao.GenerateSampleData()){
	//	log.Fatal("Error generating sample data")
	//}

	/*----------- API routes ------------*/
	router := mux.NewRouter()

	//// item API paths
	router.HandleFunc("/item/get", controller.GetAllItems).Methods("GET")
	router.HandleFunc("/item/getPage", controller.GetItems).Methods("GET")
	router.HandleFunc("/item/search", controller.SearchItems).Methods("GET")
	//router.HandleFunc("/item/{id}", controller.GetItemById).Methods("GET")
	router.HandleFunc("/item/create", controller.CreateItem).Methods("POST")
	router.HandleFunc("/item/delete/{id}", controller.DeleteItem).Methods("DELETE")
	router.HandleFunc("/item/update", controller.UpdateItem).Methods("PUT")

	// users
	router.HandleFunc("/user", controller.CreateUser).Methods("POST")
	router.HandleFunc("/user/{id}", controller.CreateUser).Methods("PUT")
	router.HandleFunc("/user", controller.GetUsers).Methods("GET")
	router.HandleFunc("/user/{id}", controller.GetUser).Methods("GET")
	router.HandleFunc("/user/{id}", controller.DeleteUser).Methods("DELETE")
	//category apis
	router.HandleFunc("/category", controller.CreateCategory).Methods("POST")
	router.HandleFunc("/category/{id}", controller.UpdateCategory).Methods("PUT")
	router.HandleFunc("/category", controller.GetAllCategory).Methods("GET")
	router.HandleFunc("/category/{id}", controller.GetCategory).Methods("GET")
	router.HandleFunc("/category/{id}", controller.DeleteCategory).Methods("DELETE")
	

	//router.HandleFunc("/user/getAll", controller.GetAllUsers).Methods("GET")
	//router.HandleFunc("/user/login", controller.Login).Methods("POST")

	//router.HandleFunc("/user/delete/{id}", controller.DeleteUser).Methods("DELETE")
	//router.HandleFunc("/user/getCart/{id}", controller.GetCartItemsByUserId).Methods("GET")

	//// cart
	//router.HandleFunc("/cart/getAll", controller.GetAllCarts).Methods("GET")
	//router.HandleFunc("/cart/{id}", controller.GetCartById).Methods("GET")
	//router.HandleFunc("/cart/create", controller.CreateCart).Methods("POST")
	////router.HandleFunc("/cart/delete/{id}", controller.DeleteCart).Methods("DELETE")
	//// cart_item
	//router.HandleFunc("/cart/addItem", controller.AddItemToCart).Methods("POST")
	//router.Headers("content-type", "application/json")

	/* handle cors */
	c := cors.New(cors.Options{
		AllowedOrigins: []string{
			"http://localhost:8080/", "http://localhost:3000", "localhost:3000/*", "http://localhost:3000/", "http://localhost:8080"},
		AllowCredentials: true,
		AllowedMethods:   []string{"GET", "HEAD", "POST", "PUT", "OPTIONS","DELETE"},
		ExposedHeaders:   []string{"content-type", "application/json"},
	})
	port := ":8080"
	log.Printf("Server is starting to listen on port  %v \n", port)
	handler := c.Handler(router)
	server := &http.Server{Addr: port, Handler: handler}
	go func() {
		errServer := server.ListenAndServe()
		if errServer != nil {
			log.Fatalf("Error while listening to the port %v \n :", errServer)
		}
	}()
	// Code to gracefully stop the server using interrupt (ctrl+c)
	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt)

	<-ch
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := server.Shutdown(ctx); err != nil {
		log.Fatalf("Error while shutting down the server : %v \n ", err)
	}

}
