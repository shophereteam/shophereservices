package modal

import "gorm.io/gorm"

type ItemTag struct {
	gorm.Model
	ItemID     uint     //fk in DB  >ID in Item struct
	CategoryID uint     //fk in DB >ID in Category struct
	Item       Item     //fk in DB  >ID in Item struct
	Category   Category //fk in DB >ID in Category struct
	IsPrimary  bool
}
