package modal

import "gorm.io/gorm"

type Cart struct {
	gorm.Model

	//UserID uint
	Items  []Item
	Status int
}
