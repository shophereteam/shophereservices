package modal

import (
	"gorm.io/gorm"
)

type Category struct {
	gorm.Model

	CategoryName string
	Status       int
	IsReturnable bool
	UpdatedBy    string
	ImageUrl     string
}
