package modal

import (
	"time"

	"gorm.io/gorm"
)

type User struct {
	gorm.Model

	Title     string
	FirstName string `gorm:"NOT NULL"`
	LastName  string `gorm:"NOT NULL"`
	Email     string `gorm:"typevarchar(100); unique_index"`
	Password  string `gorm:"typevarchar(2000);"`
	ContactNo string
	Status    int
	UserType  int
	LastLogin time.Time
	UpdatedBy time.Time
}
