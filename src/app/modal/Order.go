package modal

import (
	"time"

	"gorm.io/gorm"
)

type Order struct {
	gorm.Model

	UserID               uint
	TrackingID           string
	Status               int
	ShippingAddress      string
	PaymentStatus        int
	CouponCode           string
	PaymentProcessStatus int
	TransactionID        string
	OrderedDate          time.Time
	ShippedDate          time.Time
	DeliveredDate        time.Time
	Amount               float64
}
