package modal

import (
	"gorm.io/gorm"
)

type Address struct {
	gorm.Model

	UserID       uint
	StreetNumber int
	StreetName   string
	Unit         int
	City         string
	Province     string
	Country      string
	PostalCode   string
}
