package modal

import (
	"time"

	"gorm.io/gorm"
)

type OrderItem struct {
	gorm.Model

	OrderID         uint
	ItemID          uint
	Quantity        int8
	OrderItemStatus int
	ReturnDate      time.Time
	ReturnStatus    int
	Price           float64
}
