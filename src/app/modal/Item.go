package modal

import "gorm.io/gorm"

type Item struct {
	gorm.Model

	ItemName                 string `gorm:"not null"`
	Description              string
	ImageUrl                 string
	Price                    float64 `gorm:"check:price_checker,price>0"`
	DiscountedPrice          float64 `gorm:"check:discounted_checker,discounted_price>0"`
	PrimaryCategory          int     `gorm:"-"`
	AssociatedCategory       []int   `gorm:"-"`
	IsReturnable             bool
	Tax                      float64
	InStock                  bool
	IsFeatured               bool
	OnSale                   bool
	Status                   int        `gorm:"-"` // will not be inserted in item table
	Message                  string     `gorm:"-"` // will not be inserted in item table
	PrimaryCategoryObject    Category   `gorm:"-"`
	AssociatedCategoryObject []Category `gorm:"-"`
}

//set status 1000 only if response is Ok ie no error Otherwise use ErrorMessage.go struct so send the response.
