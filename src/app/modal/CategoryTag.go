package modal

import "gorm.io/gorm"

type CategoryTag struct {
	gorm.Model

	CategoryID           uint
	AssociatedCategoryID uint `gorm:"foreignKey:CategoryID"`
}
