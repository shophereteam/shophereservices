package utils

type ErrorMessage struct {
	Status  int
	Type    string
	Message string
}

func NewErrorMessage(status int, errorType string, message string) *ErrorMessage {
	return &ErrorMessage{status, errorType, message}
}

//////////////////////UI///////////////////////////////////
// 2000 : UI : "Invalid Data for the Item creation."
// 2001, "UI", "Invalid Data for the Item updation."
//2002, "UI", "Invalid Data for the Item Deletion."
//////////////////////DB///////////////////////////////////
// 3000 : DB : Conflicts while Item creation with existing items...
//	3001 : DB : No Records found...
//  3002, "DB", "Item does not exist..."
//  3003, "DB", "Conflicts while Item updation with existing items..."

///////////////////////APP////////////////////////////
//4000, "APP", "Invalid Item Name..."
//4001, "APP",	"Admin Access is required to make this change..."

/////////////////////////////Other/////////////////

//9000 , "UN" , "Contact support to resolve issue..."
