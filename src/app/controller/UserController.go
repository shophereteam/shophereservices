package controller

import (
	"encoding/json"
	"net/http"
	er "shop-here-api/src/app/Error"
	respHandler "shop-here-api/src/app/HTTPResponse"
	"shop-here-api/src/app/dao"
	"shop-here-api/src/app/modal"

	"github.com/gorilla/mux"
	"github.com/lib/pq"
)

func CreateUser(w http.ResponseWriter, r *http.Request) {
	var user modal.User
	json.NewDecoder(r.Body).Decode(&user)

	newUser := dao.Db.Create(&user)
	err := newUser.Error

	if err != nil {
		errmsg := ""
		if err.(*pq.Error).Code == "23505" {
			errmsg = "Unique violation. Author with email address already exists"
		}
		var errCode string = string(err.(*pq.Error).Code)

		respError := er.NewError(errCode, errmsg, err.Error())
		resp := respHandler.NewHTTPResponse(500, respHandler.EmptyResponseData(), respError)
		json.NewEncoder(w).Encode(resp)
	} else {
		data, _ := json.Marshal(user)

		//typecast it to a string
		// fmt.Println(string(data))

		resp := respHandler.NewHTTPResponse(200, respHandler.AppendResponseData(string(data), 1, 1), er.NewEmptyError())
		json.NewEncoder(w).Encode(resp)
	}

}

func UpdateUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var user modal.User

	newUser := dao.Db.First(&user, vars["id"])
	err := newUser.Error
	if newUser.RowsAffected < 1 {
		errmsg := " item does not exists"

		respError := er.NewError("500", errmsg, err.Error())
		resp := respHandler.NewHTTPResponse(500, respHandler.EmptyResponseData(), respError)
		json.NewEncoder(w).Encode(resp)
	} else {
		dao.Db.Save(&user)
		json.NewDecoder(r.Body).Decode(&user)

		// resp := respHandler.NewHTTPResponse(500, respHandler.AppendResponseData(string(data), 1, 1), err.NewEmptyError())
		// json.NewEncoder(w).Encode(user)
	}

}

func GetUsers(w http.ResponseWriter, r *http.Request) {
	var user []modal.User

	// Get generic database object sql.DB to use its functions

	newUser := dao.Db.Find(&user)
	err := newUser.Error
	if newUser.RowsAffected < 1 {
		errmsg := " records does not exists"

		respError := er.NewError("500", errmsg, err.Error())
		resp := respHandler.NewHTTPResponse(500, respHandler.EmptyResponseData(), respError)
		json.NewEncoder(w).Encode(resp)
	} else {
		data, _ := json.Marshal(user)

		resp := respHandler.NewHTTPResponse(200, respHandler.AppendResponseData(string(data), 1, 1), er.NewEmptyError())
		json.NewEncoder(w).Encode(resp)
	}

}

func GetUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var user modal.User

	newUser := dao.Db.First(&user, vars["id"])
	err := newUser.Error

	if newUser.RowsAffected < 1 {
		errmsg := " record does not exists"

		respError := er.NewError("500", errmsg, err.Error())
		resp := respHandler.NewHTTPResponse(500, respHandler.EmptyResponseData(), respError)
		json.NewEncoder(w).Encode(resp)
	} else {
		data, _ := json.Marshal(user)

		resp := respHandler.NewHTTPResponse(200, respHandler.AppendResponseData(string(data), 1, 1), er.NewEmptyError())
		json.NewEncoder(w).Encode(resp)
	}
}

func DeleteUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var user modal.User

	// db.Delete(&user, vars["id"])
	result := dao.Db.Unscoped().Where("id= ?", vars["id"]).Delete(&user)
	err := result.Error
	if result.RowsAffected < 1 {
		errmsg := " no record exists with the given id"

		respError := er.NewError("500", errmsg, err.Error())
		resp := respHandler.NewHTTPResponse(500, respHandler.EmptyResponseData(), respError)
		json.NewEncoder(w).Encode(resp)

	}
	json.NewEncoder(w).Encode("The record is deleted")
}
