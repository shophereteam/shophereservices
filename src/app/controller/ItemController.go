package controller

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	er "shop-here-api/src/app/Error"
	respHandler "shop-here-api/src/app/HTTPResponse"
	"shop-here-api/src/app/dao"
	"shop-here-api/src/app/modal"
	"shop-here-api/src/app/utils"
	"strconv"

	"github.com/gorilla/mux"
)

func CreateItem(w http.ResponseWriter, r *http.Request) {
	log.Println("Creating the item in the Create Item controller...")
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Allow-Control-Allow-Methods", "POST")
	var userItem modal.UserItem
	errIs := json.NewDecoder(r.Body).Decode(&userItem)
	if errIs != nil {
		errorMessage := utils.NewErrorMessage(2000, "UI", "Invalid Data for the Item creation.")
		json.NewEncoder(w).Encode(errorMessage)
		return
	}

	// check to make sure user is admin
	if userItem.User.UserType != 2 { // if the user is not admin return error
		errorMessage := utils.NewErrorMessage(4001, "APP", "Admin Access is required to make this change...")
		json.NewEncoder(w).Encode(errorMessage)
		return
	}

	var item = userItem.Item
	itemRepo := dao.ItemRepo{}
	itemReturn, err := itemRepo.CreateItem(item)
	if err != nil {
		log.Printf("Error in DB while creating the item: %v \n and the error is : %v \n ", item, err)
		var errorMessage *utils.ErrorMessage

		if err.Error() == "4000" {
			errorMessage = utils.NewErrorMessage(4000, "APP", "Invalid Item Name...")
		} else {
			errorMessage = utils.NewErrorMessage(3000, "DB", "Conflicts while Item creation with existing items...")
		}
		json.NewEncoder(w).Encode(errorMessage)
	} else {
		itemReturn.Status = 1000
		itemReturn.Message = fmt.Sprintf("Item creation success... with Id :%v and Item Name : %v ", itemReturn.ID, itemReturn.ItemName)
		json.NewEncoder(w).Encode(itemReturn)

	}

}

func GetAllItems(w http.ResponseWriter, r *http.Request) {
	log.Println("Getting all the items in the Item controller...")
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Allow-Control-Allow-Methods", "GET")
	itemRepo := dao.ItemRepo{}
	itemsReturn, err := itemRepo.GetAllItem()
	if err != nil {
		log.Printf("Error in DB while getting the items ,the error is : %v \n ", err)
		var errorMessage *utils.ErrorMessage

		if err.Error() == "3001" {
			errorMessage = utils.NewErrorMessage(3001, "DB", "No Records found...")
		} else {
			errorMessage = utils.NewErrorMessage(9000, "UN", "Contact support to resolve issue...")
		}
		json.NewEncoder(w).Encode(errorMessage)
	} else {
		json.NewEncoder(w).Encode(itemsReturn)
		log.Println("Items fetch success...")
	}

}

/* Pagination */
// @QueryParams(pageNo: int, pageSize: int)
func GetItems(w http.ResponseWriter, r *http.Request) {
	log.Println("Get next page of items in ItemController...")

	params := r.URL.Query()
	log.Println(params)

	// extract params & handle errors
	var pageNo, pageSize int
	var err error
	if pageNo, err = strconv.Atoi(params.Get("pageNo")); err != nil {
		pageNo = 1 // default
	}
	if pageSize, err = strconv.Atoi(params.Get("pageSize")); err != nil {
		pageSize = 10 // default
	}

	itemRepo := dao.ItemRepo{}
	itemsReturn, err := itemRepo.GetItems(pageNo, pageSize)
	if err != nil {
		log.Printf("Error in DB while getting the items ,the error is : %v \n ", err)
		var errorMessage *utils.ErrorMessage

		if err.Error() == "3001" {
			errorMessage = utils.NewErrorMessage(3001, "DB", "No Items found...")
		} else {
			errorMessage = utils.NewErrorMessage(9000, "UN", "Contact support to resolve issue...")
		}
		json.NewEncoder(w).Encode(errorMessage)
	} else {
		data, _ := json.Marshal(itemsReturn)
		resp := respHandler.NewHTTPResponse(200, respHandler.AppendResponseData(string(data), 1, 1), er.NewEmptyError())
		json.NewEncoder(w).Encode(resp)
	}

}

func SearchItems(w http.ResponseWriter, r *http.Request) {
	log.Println("Search items in ItemController")
	queryParams := r.URL.Query()

	itemRepo := dao.ItemRepo{}
	itemsRes := itemRepo.SearchItems(queryParams.Get("key"))

	data, _ := json.Marshal(itemsRes)
	resp := respHandler.NewHTTPResponse(200, respHandler.AppendResponseData(string(data), 1, 1), er.NewEmptyError())
	json.NewEncoder(w).Encode(resp)
}

func DeleteItem(w http.ResponseWriter, r *http.Request) { // delete the item for the given id in params
	log.Println("Delete items in the Item controller...")
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Allow-Control-Allow-Methods", "DELETE")
	var user modal.User
	errIs := json.NewDecoder(r.Body).Decode(&user)
	if errIs != nil {
		errorMessage := utils.NewErrorMessage(2002, "UI", "Invalid Data for the Item Deletion.")
		json.NewEncoder(w).Encode(errorMessage)
		return
	}

	if user.UserType != 2 { // if the user is not admin return error
		errorMessage := utils.NewErrorMessage(4001, "APP", "Admin Access is required to make this change...")
		json.NewEncoder(w).Encode(errorMessage)
		return
	}
	params := mux.Vars(r)
	itemRepo := dao.ItemRepo{}
	itemsReturn, err := itemRepo.DeleteItem(params["id"])
	if err != nil {
		log.Printf("Error in DB while getting the items ,the error is : %v \n ", err)
		var errorMessage *utils.ErrorMessage

		if err.Error() == "3002" {
			errorMessage = utils.NewErrorMessage(3002, "DB", "Item does not exist...")
		} else {
			errorMessage = utils.NewErrorMessage(9000, "UN", "Contact support to resolve issue...")
		}
		json.NewEncoder(w).Encode(errorMessage)
	} else {
		itemsReturn.Status = 1000
		itemsReturn.Message = "Item deletion success..."
		json.NewEncoder(w).Encode(itemsReturn)
		log.Println("Items Delete success...")
	}

}

func UpdateItem(w http.ResponseWriter, r *http.Request) { // Update the Item
	log.Println("Updating the item in the Item controller...")
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Allow-Control-Allow-Methods", "PUT")
	var userItem modal.UserItem
	errIs := json.NewDecoder(r.Body).Decode(&userItem)
	if errIs != nil {
		errorMessage := utils.NewErrorMessage(2000, "UI", "Invalid Data for the Item Updation.")
		json.NewEncoder(w).Encode(errorMessage)
		return
	}

	if userItem.User.UserType != 2 { // if the user is not admin return error
		errorMessage := utils.NewErrorMessage(4001, "APP", "Admin Access is required to make this change...")
		json.NewEncoder(w).Encode(errorMessage)
		return
	}
	var item modal.Item = userItem.Item
	// err := json.NewDecoder(r.Body).Decode(&item)
	// if err != nil {
	// 	log.Printf("Error when decoding the Json data from UI: %v \n", err)
	// 	errorMessage := utils.NewErrorMessage(2001, "UI", "Invalid Data for the Item updation.")
	// 	json.NewEncoder(w).Encode(errorMessage)
	// 	return
	// }
	itemRepo := dao.ItemRepo{}
	itemReturn, err := itemRepo.UpdateItem(item)
	if err != nil {
		log.Printf("Error in DB while updating the item: %v \n and the error is : %v \n ", item, err)
		var errorMessage *utils.ErrorMessage

		if err.Error() == "4000" {
			errorMessage = utils.NewErrorMessage(4000, "APP", "Invalid Item Name...")
		} else {
			errorMessage = utils.NewErrorMessage(3002, "DB", "Item does not exist...")
		}
		json.NewEncoder(w).Encode(errorMessage)
	} else {
		itemReturn.Status = 1000
		itemReturn.Message = fmt.Sprintf("Item updation success... with Id :%v and Item Name : %v ", itemReturn.ID, itemReturn.ItemName)
		json.NewEncoder(w).Encode(itemReturn)
		log.Printf("Item update success...")
	}

}
