package httpresponse

type httpresponseData struct {
	Data        string //json "{'key': 'value'}"
	SegmentSize int
	Size        int
}

func AppendResponseData(data string, segmentSize int, totalSize int) httpresponseData {
	return httpresponseData{data, segmentSize, totalSize}
}

func EmptyResponseData() httpresponseData {
	return httpresponseData{}
}
