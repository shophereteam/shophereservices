package httpresponse

import (
	err "shop-here-api/src/app/Error"
)

type httpresponse struct {
	Code    int
	Data    httpresponseData
	RespErr err.RError
}

func NewHTTPResponse(code int, data httpresponseData, err err.RError) httpresponse {
	return httpresponse{code, data, err}
}
